#-------------------------------------------------------------------------------
# Name:		module1
# Purpose:
#
# Author:	  Matt Edwards
#
# Created:	 30/11/2015
# Copyright:   (c) Matt Edwards 2015
# Licence:	 <your licence>
#-------------------------------------------------------------------------------

from urllib.request import urlopen
import re
import json

INDEX_URL = r"http://docs.unity3d.com/ScriptReference/docdata/index.js"

print ("Start")
indexData = urlopen (INDEX_URL).read().decode()
print ("Got It")

indexData = "{\n" + indexData
indexData = re.sub (r"^\/+.*\n", r'', indexData, flags=re.MULTILINE)
indexData = re.sub (r"var (.+) = [\r\n]+", r'"\1":', indexData)
indexData = re.sub (r"(\]|\})\;", r'\1,', indexData)
indexData = indexData[:-3] + "\n}"
indexData = indexData.replace ("\\.", ".")
indexData = indexData.replace ("\\'", "\\\\'")
print ("Processed")

index = json.loads(indexData)
print ("Parsed")

searchIndex = index['searchIndex']
pages = index['pages']
info = index['info']


def main():
	print ("Main Loop")
	print ("")
	while (True):
		terms = input("> ").split (" ")

		results = []
		for term in terms:
			for si in searchIndex:
				if si[:len(term)] == term:
					pageIndecies = searchIndex[si]
					for pageIndex in pageIndecies:
						if pageIndex not in results:
							results.append (pageIndex)

		for result in results:
			print (pages[info[result][1]][1])
			#URL
			print (pages[info[result][1]][0])
			#Summary
			print (info[result][0])
			print ("=============================")



if __name__ == '__main__':
	main()
