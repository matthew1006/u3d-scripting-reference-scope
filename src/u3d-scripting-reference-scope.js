var scopes = require('unity-js-scopes')
var http = require('http');

var unity_scripting_ref_url = "docs.unity3d.com"
var index_path = "/ScriptReference/docdata/index.js"

var description_regex = /<h2>Description<\/h2>.*([\r\n].*)+(<div class="subsection">[\r\n]+.*)?<p>((.|\r|\n)+)<\/p>/; //group $3
var example_regex = /<pre class="codeExampleCS">((.|\n|\r)+?)<\/pre>/; //group $1
var html_tag_regex = /<(.+)(.+)?>(.+)<\/\1>/gm; //group $3

var FORECAST_TEMPLATE =
        {
    "schema-version": 1,
    "template": {
        "category-layout": "grid",
        "card-layout": "horizontal",
        "card-size": "small"
    },
    "components": {
        "title": "title",
        "subtitle": "subtitle"
    }
}

var removeHTML = function (source) {
    result = source.replace (html_tag_regex, "$3");
    result = result.replace (/<br \/>/g, "\n");
    result = result.replace ("&lt;", "<");
    result = result.replace ("&gt;", ">");

    return result;
}

function getPageTitle(i) {
    return scopes.self.pages[scopes.self.info[i][1]][1];
}
function getPageURL(i) {
    return scopes.self.pages[scopes.self.info[i][1]][0];
}
function getPageSummary(i) {
    return scopes.self.info[i][0];
}

function getSearchResults(terms, query) {
    var score = [];
    var min_score = terms.length;

  for (var i = 0; i < terms.length; i++){
    var term=terms[i];
    if(scopes.self.common[term]) {
      min_score --;
    }

    if(scopes.self.searchIndex[term]) {
      for(var j = 0; j < scopes.self.searchIndex[term].length; j++) {
        var page=scopes.self.searchIndex[term][j];
        if(! score[page] )
          score[page]=0;
        ++score[page];
      }
    }

    for (var si in scopes.self.searchIndex) {
        if (si.slice (0, term.length) == term) {
            for(var j = 0; j < scopes.self.searchIndex[si].length; j++) {
                var page=scopes.self.searchIndex[si][j];
                if(! score[page] )
                    score[page]=0;
                ++score[page];
            }
        }
    }
  }
  var results = [];
  for (var page in score) {

          var title = getPageTitle(page);
          var summary = getPageSummary(page);
          var url = getPageURL(page);
    // ignore partial matches
    if(score[page] >= min_score) {
      results.push(page);

      var placement;
      // Adjust scores for better matches
      for (var i = 0; i < terms.length; i++){
        var term=terms[i];
        if( (placement=title.toLowerCase().indexOf(term)) > -1) {
          score[page] += 50;
          if(placement===0 || title[placement-1]==='.')
            score[page] += 500;
          if(placement+term.length===title.length || title[placement+term.length]==='.')
            score[page] += 500;
        }
        else if( (placement=summary.toLowerCase().indexOf(term)) > -1)
          score[page] += ((placement<10)?(20-placement):10);
      }

      if (title.toLowerCase() === query )
        score[page] += 10000;
      else if ((placement=title.toLowerCase().indexOf(query)) > -1)
        score[page] += ((placement<100)?(200-placement):100);
      else if ((placement=summary.toLowerCase().indexOf(query)) > -1)
        score[page] += ((placement<25)?(50-placement):25);
    }
  }

  results=results.sort(function (a,b) {
    if (score[b]===score[a]) { // sort alphabetically by title if score is the same
      var x = getPageTitle(a).toLowerCase();
      var y = getPageTitle(b).toLowerCase();
      return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    }
    else { // else by score descending
      return score[b]-score[a]
    }
  });

  return results;
}

scopes.self.initialize(
            {}
            ,
            {
                run: function() {
                    console.log('Running...')
                },
                start: function(scope_id) {
                    console.log('Starting scope id: '
                                + scope_id
                                + ', '
                                + scopes.self.scope_directory)
                },
                search: function(canned_query, metadata) {
                    return new scopes.lib.SearchQuery(
                                canned_query,
                                metadata,
                                // run
                                function(search_reply) {
                                    var qs = canned_query.query_string();

                                    var category_renderer = new scopes.lib.CategoryRenderer(JSON.stringify(FORECAST_TEMPLATE));
                                    var category = search_reply.register_category(" ", "", "", category_renderer);

                                    if (!qs) {

                                        var default_result = new scopes.lib.CategorisedResult(category);

                                        default_result.set_title("Search the Unity Scripting Reference.");
                                        default_result.set("subtitle", "Click on the search icon to search.");
                                        default_result.set_uri("index");

                                        search_reply.push(default_result);
                                        search_reply.finished ();
                                        return;
                                    }

                                    var get_search_index = function () {

                                        var get_index = function(response) {
                                            var res = '';

                                            // Another chunk of data has been recieved, so append it to res
                                            response.on('data', function(chunk) {
                                                res += chunk;
                                            });

                                            // The whole response has been recieved
                                            response.on('end', function() {

                                                res = res.replace (/^\/{2}.*[\r\n]+/gm, "");
                                                res = "{\n" + res;
                                                res = res.replace (/var (.+) = [\r\n]+/gm, '\"$1\":');
                                                res = res.replace (/(\]|\})\;/gm, '$1,');
                                                res = res.slice (0,-3) + "\n}";
                                                res = res.replace ("\\.", ".");
                                                res = res.replace (/\\([^\"])/gm, "\\\\$1");
                                                res = res.replace ("&gt;", ">");

                                                r = JSON.parse(res);

                                                scopes.self.searchIndex = r.searchIndex;
                                                scopes.self.pages = r.pages;
                                                scopes.self.info = r.info;
                                                scopes.self.common = r.common;

                                                console.log ("Ready");
                                                do_search ();
                                            });
                                        };

                                        // Now call back into the API for a 7 day forecast
                                        http.request({host: unity_scripting_ref_url, path: index_path}, get_index).end();
                                    };

                                    var do_search = function ()
                                    {
                                        try
                                        {
                                            qs = qs.toLowerCase ();
                                            var terms = qs.split (/[\s\.]/);
                                            terms.push (qs.replace (/\s+/, ""));
                                            var results = getSearchResults (terms, qs)

                                            var cardinality = metadata.cardinality();

                                            for (var k = 0; k < results.length && (cardinality === 0 || k < cardinality); k++)
                                            {
                                                var page =  results[k];

                                                var categorised_result = new scopes.lib.CategorisedResult(category);

                                                categorised_result.set_title(scopes.self.pages[scopes.self.info[page][1]][1]);
                                                categorised_result.set("subtitle", scopes.self.info[page][0]);
                                                categorised_result.set_uri(scopes.self.pages[scopes.self.info[page][1]][0]);

                                                search_reply.push(categorised_result);
                                            }
                                        }
                                        catch(e) {
                                            // Forecast not available
                                            console.log("Forecast for '" + qs + "' is unavailable: " + e)
                                        }
                                        finally
                                        {
                                            search_reply.finished ();
                                        }
                                    }

                                    if (scopes.self.searchIndex === undefined)
                                    {
                                        get_search_index ();
                                    }
                                    else
                                    {
                                        do_search ();
                                    }


                                },
                                // cancelled
                                function() {
                                });
                },
                preview: function(result, action_metadata) {
                    return new scopes.lib.PreviewQuery(
                                result,
                                action_metadata,
                                // run
                                function(preview_reply) {

                                    var r = this.result();

                                    var path = "/ScriptReference/" + r.get("uri") + ".html";

                                    var get_preview = function(response) {
                                        var res = '';

                                        // Another chunk of data has been recieved, so append it to res
                                        response.on('data', function(chunk) {
                                            res += chunk;
                                        });

                                        // The whole response has been recieved
                                        response.on('end', function() {


                                            var desc = description_regex.exec(res);
                                            if (desc !== null)
                                            {
                                                desc = removeHTML (desc[3]);
                                            }

                                            var exam = example_regex.exec(res);
                                            if (exam !== null)
                                            {
                                                exam = removeHTML (exam[1]);
                                            }

                                            var layout1col = new scopes.lib.ColumnLayout(1);
                                            var layout2col = new scopes.lib.ColumnLayout(2);
                                            var layout3col = new scopes.lib.ColumnLayout(3);
                                            layout1col.add_column(["header", "description", "action"]);

                                            layout2col.add_column(["header", "description", "action"]);
                                            layout2col.add_column(["example"]);

                                            layout3col.add_column(["header", "description", "action"]);
                                            layout3col.add_column(["example"]);
                                            layout3col.add_column([]);

                                            preview_reply.register_layout([layout1col, layout2col, layout3col]);

                                            var header = new scopes.lib.PreviewWidget("header", "header");
                                            header.add_attribute_mapping("title", "title");

                                            var description = new scopes.lib.PreviewWidget("description", "text");
                                            description.add_attribute_value("text", desc || "View online at http://" + unity_scripting_ref_url + path);

                                            var example = new scopes.lib.PreviewWidget("example", "text");
                                            example.add_attribute_value("text", exam);

                                            var action = new scopes.lib.PreviewWidget("action", "actions");
                                            action.add_attribute_value(
                                                            "actions",
                                                            {
                                                                "id": "more",
                                                                "label": "More",
                                                                "uri": "http://" + unity_scripting_ref_url + path
                                                            }
                                                        );

                                            preview_reply.push([header, description, example, action]);
                                            preview_reply.finished();
                                        });
                                    };

                                    // Now call back into the API for a 7 day forecast
                                    http.request({host: unity_scripting_ref_url, path: path}, get_preview).end();


                                },
                                // cancelled
                                function() {
                                });
                }
            }
            );

